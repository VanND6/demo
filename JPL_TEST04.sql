create database JPL_TEST04
go
use JPL_TEST04
go
create table Department(
dept_no int primary key,
dept_name varchar(50),
[description] varchar(100)
)
go
create table Employee(
emp_no int primary key,
birth_date varchar(10),
first_name varchar(50),
last_name varchar(50),
gender varchar(10),
hire_date varchar(10)
)
go
create table Working_History(
dept_no int foreign key references Department(dept_no),
emp_no int foreign key references Employee(emp_no),
from_date varchar(10) ,
to_date varchar(10),
primary key(dept_no,emp_no)
)
go

insert into  Department values  (1,'CNTT','CONG NGHE THONG TIN'),
                                (2,'KTPM','KI THUAT PHAN MEM'),
								(3,'ATTT','AN TOAN THONG TIN')

insert into Employee values (1,'1/2/2021','tran','anh','M','5/12/2022'),
                            (2,'8/5/2021','minh','cong','M','7/11/2022'),
							(3,'3/3/2021','ngoc','lan','F','9/22/2022')


insert into Working_History values (1,1,'1/1/2021','10/9/2022'),
                                   (2,2,'2/2/2021','11/10/2022'),
								   (3,3,'3/3/2021','12/11/2022')

								   select*from Employee where emp_no =1